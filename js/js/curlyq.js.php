$(function () {
    $('.request-link').on('click', function (event) {
        $('#request-input').val($(this).attr('href') + '.json');

        $('#request-form').submit();

        event.preventDefault();
    });

    $('#request-form').on('submit', function (event) {
        $.getJSON($('#request-input').val(), {}, function (response) {
            $('#response-textarea').val(JSON.stringify(response, null, 2));
        }).error(function () {
            $('#response-textarea').val('query failed :-(');
        });

        event.preventDefault();
    });
});
