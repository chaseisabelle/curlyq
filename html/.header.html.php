<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8">
        <title>curlyq</title>
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" />
        <link href="/css/curlyq.css" rel="stylesheet" />
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/curlyq.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <center>
                            <h1>curlyq</h1>
                        <center>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="hidden-xs hidden-sm">
                                <?php foreach (get_menus() as $menu) if (empty($menu)) { ?><hr /><?php } else { ?><li>
                                    <strong>
                                        <?php if (!empty($menu['url'])) { ?><a href="<?php print $menu['url']; ?>"><?php } ?><?php print $menu['title']; if (!empty($menu['url'])) { ?></a><?php } ?>
                                    </strong>
                                    <?php if (!empty($menu['items'])) { ?><ul>
                                        <?php foreach ($menu['items'] as $item) { ?><li>
                                            <a href="<?php print $item['url']; ?>" class="request-link"><?php print $item['title']; ?></a>
                                        </li><?php } ?>
                                    </ul><?php } ?>
                                </li><?php } ?>
                            </ul>
                            <ul class="hidden-md hidden-lg">
                                <?php foreach (get_menus() as $menu) if (empty($menu)) { ?><hr /><?php } else { ?>
                                    <?php if (!empty($menu['url'])) { ?><li>
                                        <a href="<?php print $menu['url']; ?>"><?php print $menu['title']; ?></a>
                                    </li><?php } ?>
                                    <?php if (!empty($menu['items']))  foreach ($menu['items'] as $item) { ?><li>
                                        <a href="<?php print $item['url']; ?>" class="request-link"><?php print $menu['title']; ?>/<?php print $item['title']; ?></a>
                                    </li><?php } ?>
                                </li><?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
