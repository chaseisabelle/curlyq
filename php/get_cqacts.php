<?php
function get_cqacts($cqcon) {
    $cqacts = scan_dir(get_cqdir() . $cqcon);

    foreach ($cqacts as $key => $cqact) {
        $cqacts[$key] = preg_remove('/\.sql$/', $cqact);
    }

    return $cqacts;
}
?>
