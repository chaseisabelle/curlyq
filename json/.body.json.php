<?php
$request = [];

foreach ($_REQUEST as $key => $value) {
    if (!in_array($key, ['PHPSESSID'])) {
        $request[':' . $key] = $value;
    }
}

$response = pdo_query(pdo_open(), file_read(__DIR__ . '/../sql/' . controller() . '/' . action() . '.sql'), $request);

if (!is_array($response)) {
    $response = ['response' => $response];
}

json_dump($response);
?>
