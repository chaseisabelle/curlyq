<?php  set_error_handler(function ($code, $error, $file, $line) {
if (!defined('FLOG_ID')) {
define('FLOG_ID', uniqid());}    $exception = new Exception($error);$exception->__ERR__ = [
'id'    => FLOG_ID,
'error' => $error,
'code'  => $code,
'file'  => $file,
'line'  => $line,
'trace' => debug_backtrace()
];throw $exception;}, E_ALL | E_NOTICE | E_STRICT | E_WARNING);set_exception_handler(function ($exception) {
if (!defined('FLOG_ID')) {
define('FLOG_ID', uniqid());}    $error = isset($exception->__ERR__) ? $exception->__ERR__ : [
'id'    => FLOG_ID,
'error' => $exception->getMessage(),
'code'  => $exception->getCode(),
'file'  => $exception->getFile(),
'line'  => $exception->getLine(),
'trace' => $exception->getTrace()
];$trace = '';foreach (isset($error['trace']) ? $error['trace'] : [] as $entry) {
if (!isset($entry['file'])) {
$entry['file'] = '';}        if (!isset($entry['line'])) {
$entry['line'] = 0;}        if (!isset($entry['args'])) {
$entry['args'] = [];}        $trace .= $entry['file'] . ':' . $entry['line'] . ' ' . $entry['function'] . '(';foreach ($entry['args'] as $key => $arg) {
$entry['args'][$key] = function_exists('spy') ? spy($arg) : var_dump($arg);}        $trace .= implode(', ', $entry['args']) . ")\n";}    $trace = rtrim($trace);switch (1) {
case function_exists('is_www') && is_www():
error_log(FLOG_ID . ' ' . $error['file'] . ':' . $error['line'] . ' (' . $error['code'] . ') ~ ' . $error['error']);while (ob_get_level()) {
ob_end_clean();}            ob_start('ob_gzhandler');switch (content_ext()) {
case 'json':
die(json_encode($error));case 'xml':
die('<error>' . $error['error'] . '</error>');case 'txt':
die($error['id'] . ' ' . $error['file'] . ':' . $error['line'] . ' (' . $error['code'] . ') ~ ' . $error['error']);case 'js':
die('if(typeof console.log != "undefined")console.log("' . $error['id'] . ' ' . $error['file'] . ':' . $error['line'] . ' (' . $error['code'] . ') ~ ' . $error['error'] . '")');case 'css':
die('');case 'html':
default:
}            die('<html><body><pre>' . $error['id'] . ' ' . $error['file'] . ':' . $error['line'] . ' (' . $error['code'] . ') ~ ' . $error['error'] . "\n" . $trace . '</pre></body></html>');case function_exists('is_cli') && is_cli():
default:
print $error['id'] . ' ' . $error['file'] . ':' . $error['line'] . ' (' . $error['code'] . ') ~ ' . $error['error'] . "\n" . $trace;}
});date_default_timezone_set('UTC');define('FLOG_ID',     uniqid());define('ERROR_DATA',  uniqid());define('LIBS',        'soundcloud,imgur,facebook,twitter,photobucket,youtube,giphy,google,mandrill,instagram,bitbucket');define('PHP_INT_MIX', ~PHP_INT_MAX);$_ = [];if (!defined('CFG_DIR')) {
define('CFG_DIR', '/var/cfg');}if (defined('APP_NAME') && (is_readable($_['cfg'] = __DIR__ . CFG_DIR . '/' . APP_NAME . '.php') || is_readable($_['cfg'] = CFG_DIR . '/' . APP_NAME . '.cfg.php'))) {
include_once($_['cfg']);}foreach (scandir(__DIR__) as $_['php']) {
if (preg_match('/^\w+\.php$/i', $_['php'])) {
include_once __DIR__ . '/' . $_['php'];}
}foreach ([
'SQL_FLOG'  => '/var/log/sql.log',
'CURL_FLOG' => '/var/log/curl.log',
'APP_DIR'   => $_['tmp'] = (defined('APP_NAME') ? '/var/' . (is_www() ? 'www' : 'cli') . '/' . APP_NAME : __DIR__),
'PHP_DIR'   => $_['tmp'] . '/php'
] as $_['cfg'] => $_['val']) {
if (!defined($_['cfg'])) {
define($_['cfg'], $_['val']);}
}if (is_www()) {
ob_start('ob_gzhandler');session_start();if (!array_key_exists(APP_NAME, $_COOKIE)) {
$_COOKIE[APP_NAME] = '{}';}    try {
$_COOKIE = json_to_array($_COOKIE[APP_NAME]);} catch (Exception $exception) {
$_COOKIE = [];}    unset($exception);register_shutdown_function(function () {
header('Content-Type: ' . content_type());while (ob_get_level()) {
ob_end_flush();}
});if (!defined('CONTENT_DIR')) {
define('CONTENT_DIR', '/var/www/' . APP_NAME . '/' . content_ext());}    foreach ([PHP_DIR, $_ = PHP_DIR . '/' . controller(), $_ . '/' . action()] as $_) {
if (is_dir($_)) {
require_dir($_);}
}    include_if_exists(dir_trim(CONTENT_DIR) . '/.header.' . content_ext() . '.php');try { //<< controller
ob_start();include_if_exists(dir_trim(CONTENT_DIR) . '/' . controller() . '/.header.' . content_ext() . '.php');try { //<< action
ob_start();if (
file_exists($_ = dir_trim(CONTENT_DIR) . '/' . controller() . '/' . action() . '.' . content_ext() . '.php') ||
file_exists($_ = dir_trim(CONTENT_DIR) . '/' . controller() . '/.body.' . content_ext() . '.php') ||
file_exists($_ = dir_trim(CONTENT_DIR) . '/.body.' . content_ext() . '.php') && !file_exists(dir_trim(CONTENT_DIR) . '/' . controller())
) {
include $_;} else {
header('HTTP/1.0 404 Not Found');trigger_error('404 Not Found (' . implode('/', controller_and_action()) . ').');}            ob_end_flush();} catch (Exception $_) { //<< action fail
ob_end_clean();$_ = ['exception' => $_];if (
file_exists($_['content'] = dir_trim(CONTENT_DIR) . '/' . controller() . '/' . action() . '/.error.' . content_ext() . '.php') ||
file_exists($_['content'] = dir_trim(CONTENT_DIR) . '/' . controller() . '/.error.' . content_ext() . '.php')
) {
$GLOBALS[ERROR_DATA] = isset($_['exception']->__ERR__) ? $_['exception']->__ERR__ : [
'error' => $_['exception']->getMessage(),
'code'  => $_['exception']->getCode(),
'file'  => $_['exception']->getFile(),
'line'  => $_['exception']->getLine(),
'trace' => $_['exception']->getTrace()
];include $_['content'];} else {
throw $_['exception'];}
}        include_if_exists(dir_trim(CONTENT_DIR) . '/' . controller() . '/.footer.' . content_ext() . '.php');ob_end_flush();} catch (Exception $_) { //<< controller fail
ob_end_clean();$_ = ['exception' => $_];if (file_exists($_['content'] = dir_trim(CONTENT_DIR) . '/.error.' . content_ext() . '.php')) {
$GLOBALS[ERROR_DATA] = isset($_['exception']->__ERR__) ? $_['exception']->__ERR__ : [
'error' => $_['exception']->getMessage(),
'code'  => $_['exception']->getCode(),
'file'  => $_['exception']->getFile(),
'line'  => $_['exception']->getLine(),
'trace' => $_['exception']->getTrace()
];include $_['content'];} else {
throw $_['exception'];}
}
include_if_exists(dir_trim(CONTENT_DIR) . '/.footer.' . content_ext() . '.php');}unset($_);  function action() {
return controller_and_action()['action'];}  function action_exists($a, $c = null) {
if (func_num_args() === 1) {
$c = controller();}    return controller_exists($c) && in_array($a, actions());}  function actions($controller) {
$actions = [];if (is_dir($path = CONTENT_DIR . '/' . $controller)) {
foreach (scan_dir($path) as $file) {
if (preg_match('/[\w-]+\.php$/', $file)) {
$actions[] = preg_remove('/\.php$/', $file);}
}
}    return $actions;}  function get_alphabet($uppercase = false) {
$alphabet = [];for ($code = ($uppercase ? 65 : 97);$code <= ($uppercase ? 90 : 122);$code++) {
$alphabet[$code] = chr($code);}    return $alphabet;}  function array_append($array, $key, $value) {
$inc = 0;while (array_key_exists($add = $key . (++$inc < 2 ? '' : $inc), $array));$array[$add] = $value;return $array;}  function array_avg($a) {
return ($sum = array_sum($a)) ? $sum / count($a) : 0;}  function array_body($array, $from, $to) {
return $array ? array_slice($array, $offset = array_index($array, $from), array_index($array, $to) - $offset + 1, 1) : [];}  function array_cat($a, $b) {
foreach (array_tail(func_get_args(), 1) as $b) {
foreach ($b as $k => $v) {
$a = array_append($a, $k, $v);}
}    return $a;}  function array_derive($array, $deriver = null) {
if (func_num_args() < 2) {
$deriver = function ($a, $b) {
return $b - $a;};}    $derivation = [];foreach (array_chunk($array, 2, 1) as $chunk) {
if (isset($last, $lkey)) {
$derivation[strval((array_first_key($chunk) - $lkey) / 2 + $lkey)] = $deriver($last, array_first($chunk));}        if (count($chunk) === 2) {
$derivation[strval((($lkey = array_last_key($chunk)) - array_first_key($chunk)) / 2 + array_first_key($chunk))] = $deriver(array_first($chunk), $last = array_last($chunk));}
}    return $derivation;}  function array_first($a, $d = null) {
if (count($a)) {
return array_shift($a);}    if (func_num_args() > 1) {
return $d;}    trigger_error('Cannot get first element of empty array ' . spy($a) . '.');}  function array_first_key($a, $d = null) {
$a = array_keys($a);return func_num_args() > 1 ? array_first($a, $d) : array_first($a);}  function array_frequency($array, $format = 'seconds') {
return calculate_frequency(count($array), min($array), max($array), $format);}  function array_get($a, $k, $d) {
return array_key_exists($k, $a) ? $a[$k] : $d;}  function array_head($array, $to) {
return array_body($array, array_first_key($array), $to);}  function array_index($array, $key) {
foreach (array_keys($array) as $index => $value) {
if ($key === $value) {
return $index;}
}    trigger_error('Failed to get index of ' . spy($key) . ' in ' . spy($array) . '.');}  function array_indexed($array) {
for ($index = 0;$index < count($array);$index++) {
if (!array_key_exists($index, $array)) {
return false;}
}    return true;}  function array_key($array, $value, $strict = 1) {
foreach ($array as $key => $element) {
if ($strict && $element === $value || !$strict && $element == $key) {
return $key;}
}    trigger_error('No key found for ' . spy($value) . ' in ' . spy($array) . '.');}  function array_key_filter($array, $filter) {
foreach ($array as $key => $value) {
if (!call_user_func($filter, $key)) {
unset($array[$key]);}
}    return $array;}  function array_keys_exists($array, $keys) {
foreach ($keys as $key) {
if (!array_key_exists($key, $array)) {
return false;}
}    return true;}  function array_krsort($array, $flags = SORT_REGULAR) {
if (!krsort($array, $flags)) {
trigger_error('Failed to sort ' . spy($array) . ' by keys in reverse.');}    return $array;}  function array_ksort($array, $flags = SORT_REGULAR) {
if (is_callable($flags) && uksort($array, $flags) || !is_callable($flags) && !ksort($array, $flags) || !is_callable($flags) && !is_whole($flags)) {
trigger_error('Failed to sort ' . spy($array) . ' by keys.');}    return $array;}  function array_last($a, $d = null) {
if (count($a)) {
return array_pop($a);}    if (func_num_args() > 1) {
return $d;}    trigger_error('Cannot get last element of empty array ' . spy($a) . '.');}  function array_last_key($a, $d = null) {
$a = array_keys($a);return func_num_args() > 1 ? array_last($a, $d) : array_last($a);}  function array_ltrim($array, $mask = " \t\n\r\0\x0B") {
foreach ($array as $key => $value) {
$array[$key] = ltrim($value, $mask);}
return $array;}  function array_rand_value($a) {
return $a[array_rand($a)];}  function array_rsort($array, $flags = SORT_REGULAR) {
return array_reverse(array_sort($array, $flags));}  function array_rtrim($array, $mask = " \t\n\r\0\x0B") {
foreach ($array as $key => $value) {
$array[$key] = rtrim($value, $mask);}
return $array;}  function array_set_first($array, $key) {
$first = $array[$key];unset($array[$key]);return array_merge([$key => $first], $array);}  function array_set_last($array, $key) {
$last = $array[$key];return array_merge($array, [$key => $last]);}  function array_shuffle($array) {
if (!shuffle($array)) {
trigger_error('Failed to shuffle array ' . spy($array) . '.');}    return $array;}  function array_sort($array, $flags = SORT_REGULAR) {
if (is_callable($flags) && !uasort($array, $flags) || !is_callable($flags) && !asort($array, $flags) || !is_callable($flags) && !is_whole($flags)) {
trigger_error('Failed to sort array ' . spy($array) . '.');}    return $array;}  function array_tail($array, $from) {
return array_body($array, $from, array_last_key($array));}  function array_to_json($a) {
if (!is_string($json = json_encode($a))) {
trigger_error('Failed to convert ' . spy($a) . ' to JSON.');}    return $json;}  function array_to_xml($array) {
return json_to_xml(array_to_json($array));}  function array_trim($array, $mask = " \t\n\r\0\x0B") {
foreach ($array as $key => $value) {
$array[$key] = trim($value, $mask);}
return $array;}  function array_union($a, $b) {
if (func_num_args() > 2) {
$b = func_get_args();array_shift($b);foreach ($b as $b) {
$a = array_union($a, $b);}        return $a;}        foreach ($b as $k => $v) {
$a = array_append($a, $k, $v);}    return $a;}  function ask($question, $answer, $case = 0) {
return !call_user_func('str' . ($case ? '' : 'case') . 'cmp', prompt($question), $answer);}    function asql_set($pdo, $table, $params, $id = null) {
if (func_num_args() < 4) {
return asql_get(
$pdo,
$table,
array_keys($params),
pdo_query($pdo, 'INSERT INTO `' . $table . '` (`' . implode('`, `', array_keys($params)) . '`) VALUES (' . implode(', ', str_split(str_repeat('?', count($params)))) . ')', $params)
);}    $fields = [];foreach ($params as $field => $value) {
$fields[] = '`' . $field . '` = ?';}
pdo_query($pdo, 'UPDATE `' . $table . '` SET ' . implode(', ', $fields) . ' WHERE id = ?', $params + [$id]);return asql_get($pdo, $table, array_keys($params), $id);}  function bitbucket_close(&$bitbucket) {
curl_close($bitbucket);destroy($bitbucket);}  function bitbucket_open($key = BITBUCKET_KEY, $secret = BITBUCKET_SECRET, $username = BITBUCKET_USERNAME, $password = BITBUCKET_PASSWORD) {
$bitbucket = curl_open();return curl_set_options($bitbucket, [
CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
CURLOPT_USERPWD        => $username . ':' . $password,
CURLOPT_SSL_VERIFYPEER => 0,
CURLOPT_SSL_VERIFYHOST => 0,
CURLOPT_HTTPHEADER     => ['Content-Type: application/json']
]);}  function bitbucket_query($bitbucket, $service, $request = [], $method = 'GET', $version = '1.0') {
curl_set_method($bitbucket, $method);curl_set_request($bitbucket, $request);$response = curl_query(curl_set_url($bitbucket, 'https://api.bitbucket.org/' . $version . '/' . $service));try {
switch ($code = curl_get_code($bitbucket)) {
case 400:
trigger_error('Bad request.');case 401:
trigger_error('Authentication failure.');case 403:
trigger_error('Forbidden.');case 404:
trigger_error('Service not found.');case 204:
$response = '{}';case 200:
case 201:
break;default:
trigger_error('Received response code ' . spy($code) . '.');}
} catch (Exception $exception) {
trigger_error('Bitbucket says ' . spy($exception->getMessage()) . '.');}    return json_to_array($response);}  function bitbucket_repository_changesets($bitbucket, $username, $repository, $limit = 5) {
$service = 'repositories/' . $username . '/' . $repository . '/changesets?limit=' . $limit;if (func_num_args() >= 5) {
$service .= '&node=' . func_get_arg(4);}    return bitbucket_query($bitbucket, $service);}  function bitbucket_user($bitbucket) {
return bitbucket_query($bitbucket, 'user');}  function bitbucket_user_url($bitbucket, $username = BITBUCKET_USERNAME) {
return 'http://bitbucket.org/' . $username;}  function bitbucket_users($bitbucket, $username = BITBUCKET_USERNAME, $service = '', $request = [], $method = 'GET') {
return bitbucket_query($bitbucket, 'users/' . $username . '/' . $service, $request, $method);}  function bitbucket_users_emails($bitbucket, $username = BITBUCKET_USERNAME, $email = '', $request = []) {
switch (1) {
case is_string($request):
$request = ['email' => $request];$method = 'POST';break;case is_array($request):
$method = 'GET';break;default:
$request = ['primary' => $request ? true : false];$method = 'PUT';}    return bitbucket_users($bitbucket, $username, 'emails/' . $email, $request, $method);}  function bitbucket_users_events($bitbucket, $username = BITBUCKET_USERNAME) {
return bitbucket_users($bitbucket, $username, 'events');}  function bitbucket_users_followers($bitbucket, $username = BITBUCKET_USERNAME) {
return bitbucket_users($bitbucket, $username, 'followers');}    function bitbucket_users_plan($bitbucket, $username = BITBUCKET_USERNAME) {
return bitbucket_users($bitbucket, $username, 'plan');}  function build_path($dir, $file) {
return dir_trim($dir) . '/' . $file;}  function calculate_frequency($count, $from, $to, $format = 's') {
$interval = calculate_interval($from, $to, $format);if (!$interval) {
return INF;}    return $count / $interval;}  function calculate_percentage($part, $whole) {
return $whole ? $part / $whole * 100 : 0;}  function const_get($name, $default) {
if (!defined($name)) {
return $default;}    return constant($name);}  function content_ext() {
if (preg_match('/\.(?P<ext>\w+)$/i', parse_url(trim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH), $matches) && !empty($matches['ext'])) {
return strtolower($matches['ext']);}    return 'html';}  function content_exts() {
return [
'html',
'json',
'xml',
'txt',
'js',
'css'
];}  function content_type() {
switch (content_ext()) {
case 'json':
return 'application/json';case 'xml':
return 'text/xml';case 'txt':
return 'text/plain';case 'js':
return 'application/javascript';case 'css':
return 'text/css';case 'html':
default:
return 'text/html';}    trigger_error('IDK WTF happned :-(');}  function controller() {
return controller_and_action()['controller'];}  function controller_and_action() {
if (!is_array($_['uri'] = explode('/', ($_['uri'] = parse_url(trim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH))))) {
trigger_error('Failed to parse requested URI ' . spy($_SERVER['REQUEST_URI']) . '.');}    if (string_empty($_['uri'][0])) {
unset($_['uri'][0]);}    return [
'controller' => isset($_['uri'][0]) ? preg_remove('/\.\w+$/i', $_['uri'][0]) : 'index',
'action'     => isset($_['uri'][1]) ? preg_remove('/\.\w+$/i', $_['uri'][1]) : 'index'
];}  function controller_exists($c) {
return in_array($c, controllers());}  function controllers() {
$controllers = [];foreach (scan_dir(CONTENT_DIR) as $path => $file) {
if (is_dir($path) && preg_match('/^[\w-]+$/', $file) && $file !== APP_NAME) {
$controllers[] = $file;}
}    return $controllers;}  function conv_time($units, $from, $to) {
return from_secs(to_secs($units, $from), $to);}  function convert_numeric_base($n, $f, $t) {
return floatval(base_convert($n, $f, $t));}  function cookie_get($k, $d) {
return array_get($_COOKIE, $k, $d);}  function css_minify($css) {
return $css;}  function curl_enabled() {
return function_exists('curl_version');}  function curl_flog($msg) {
flog(CURL_FLOG, $msg);}  function curl_get_code($curl) {
return curl_get_info($curl, CURLINFO_HTTP_CODE);}  function curl_get_content_type($curl) {
if (is_null($content_type = curl_get_info($curl, CURLINFO_CONTENT_TYPE))) {
trigger_error('Failed to determine document content type for ' . spy($curl) . '.');}    return $content_type;}  function curl_get_info($curl, $option = 0) {
if (is_bool($info = curl_getinfo($curl, $option))) {
trigger_error('Failed to get ' . spy($option) . ' for ' . spy($curl) . '.');}    return $info;}  function curl_get_url($curl) {
return curl_get_info($curl, CURLINFO_EFFECTIVE_URL);}  function curl_open($url = null, $request = []) {
require_curl('cURL library not enabled.');if (!$curl = curl_init($url . ($request ? '?' . http_build_query($request) : ''))) {
trigger_error('Failed to open cURL resource for ' . spy($url . $request) . '.');}    return curl_set_options($curl, [CURLOPT_RETURNTRANSFER => 1, CURLOPT_TIMEOUT => 60]);}  function curl_query($curl) {
curl_flog(spy($curl) . ' ' . curl_get_url($curl));if (!is_string($response = curl_exec($curl))) {
trigger_error('Failed to execute cURL query for ' . spy($curl) . '.');}    curl_flog(curl_get_code($curl) . ' ' . substr($response, 0, 80) . '...');return $response;}  function curl_require_code($curl, $code, $error = null) {
if (curl_get_code($curl) === intval($code)) {
return;}    if (func_num_args() < 3) {
$error = spy(curl_get_url($curl)) . ' responded with code ' . spy(curl_get_code($curl)) . ' instead of code ' . spy($curl) . '.';}    trigger_error($error);}  function curl_set_header(&$curl, $header) {
return curl_set_headers($curl, [$header]);}  function curl_set_headers(&$curl, $headers = []) {
return $headers ? curl_set_option($curl, CURLOPT_HTTPHEADER, $headers) : $curl;}  function curl_set_method(&$curl, $method = 'GET') {
return curl_set_option($curl, CURLOPT_CUSTOMREQUEST, $method);}  function curl_set_option(&$curl, $option, $value) {
return curl_set_options($curl, [$option => $value]);}  function curl_set_options(&$curl, $options) {
if (!curl_setopt_array($curl, $options)) {
trigger_error('Failed to initialize cURL options for ' . spy($curl) . '.');}    return $curl;}  function curl_set_request(&$curl, $request, $post = true) {
if ($post) {
return curl_set_option($curl, CURLOPT_POSTFIELDS, $request);}    return curl_set_url($curl, url_strip(curl_get_url($curl)) . '?' . http_build_query(array_merge(url_request(curl_get_url($curl)), $request)));}  function curl_set_url(&$curl, $url) {
return curl_set_option($curl, CURLOPT_URL, $url);}  function destroy(&$var) {
$var = null;unset($var);}  function dictionary_close(&$dictionary) {
destroy($dictionary);}  function dictionary_open() {
return gzcompress(file_read(__DIR__ . '/.dictionary/.txt'));}  function dictionary_word($w) {
return dictionary()[strtolower($w)];}  function dictionary_word_exists($w) {
return in_array(strtolower($w), dictionary_words());}  function dictionary_words($d) {
return explode("\n", gzuncompress($d));}  function dir_trim($dir) {
return rtrim($dir, '/');}  function error_code() {
return intval(error_data()['code']);}  function error_data() {
return array_get($GLOBALS, ERROR_DATA, [
'error' => 'No errors.',
'file'  => '',
'line'  => 0,
'code'  => 0,
'trace' => []
]);}  function error_dump($buffer = 0, $trace = 1) {
$dump = error_file() . ':' . error_line() . ' (' . error_code() . ') ' . error_message();if ($trace) {
$dump .= "\n" . error_trace_string();}    if ($buffer) {
return $dump;}    print $dump;}  function error_file() {
return error_data()['file'];}  function error_id() {
return FLOG_ID;}  function error_line() {
return intval(error_data()['line']);}  function error_message() {
return error_data()['error'];}  function error_trace() {
return error_data()['trace'];}  function error_trace_args($t, $spy = 0) {
$args = array_get($t, 'args', []);if ($spy) {
foreach ($args as $key => $arg) {
$args[$key] = spy($arg);}
}    return $args;}  function error_trace_file($trace) {
return array_get($trace, 'file', '?');}  function error_trace_function($t) {
return array_get($t, 'function', '?');}  function error_trace_line($t) {
return array_get($t, 'line', '?');}  function error_trace_string() {
$string = error_file() . ':' . error_line() . ' (' . error_code() . ') ' . error_message() . "\n";foreach (error_trace() as $trace) {
$string = error_trace_file($trace) . ':' . error_trace_line($trace)  . ' ' . error_trace_function($trace) . '(' . implode(', ', error_trace_args($trace, 1)) . ')' . "\n";}    return trim($string);}  function execute($command) {
if (!is_array($command)) {
if (is_null($output = shell_exec($command))) {
trigger_error('Failed to execute ' . spy($command) . '.');}        return $output;}    foreach ($command as $k => $c) {
$command[$k] = execute($c);}    return implode("\n", $command);}  function fb_auth($fb, $scope = []) {
if (!fb_authed($fb)) {
redirect($fb->getLoginUrl($scope));}    return $fb;}  function fb_authed($fb) {
return $fb->fbsess ? true : false;}  function fb_close(&$fb) {
destroy($fb);}  function fb_open($id = FB_ID, $secret = FB_SECRET, $redirect = null) {
define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/.fb');include(__DIR__ . '/.fb/autoload.php');Facebook\FacebookSession::setDefaultApplication($id, $secret);if (func_num_args() < 3) {
$redirect = defined('FB_REDIRECT') ? FB_REDIRECT : server_root(1);}    $fb = new Facebook\FacebookRedirectLoginHelper($redirect);$fb->fbsess = $fb->getSessionFromRedirect();return $fb;}  function fb_query($fb, $fields = '', $uri = '/me', $method = 'GET') {
if ($fields) {
$uri .= '?fields=' . $fields;}    return json_to_array((new Facebook\FacebookRequest($fb->fbsess, $method, $uri))->execute()->getRawResponse());}  function fb_unauth($fb, $redirect = null) {
if (!fb_authed()) {
return;}    if (func_num_args() < 2) {
$redirect = server_root(1);}    redirect($fb->getLogoutUrl($redirect));}  function file_append($file, $data, $delimiter = '') {
return file_write($file, $data . $delimiter, FILE_APPEND);}  function file_close(&$file) {
if (!fclose($file)) {
trigger_error('Failed to close file ' . spy($file) . '.');}    $file = null;unset($file);}  function file_get_contents_if_exists($file, $default = '') {
return file_exists($file) ? file_get_contents($file) : $default;}  function file_read($file, $default = null) {
if (!file_exists($file) && func_num_args() > 1) {
return $default;}    if (!is_string($data = file_get_contents($file))) {
trigger_error('Failed to read ' . spy($file) . '.');}    return $data;}  function file_write($file, $data, $flags = 0) {
if (file_put_contents($file, $data, $flags) !== strlen($data)) {
trigger_error('Failed to write ' . spy($data) . ' to ' . spy($file) . '.');}
}  function flog($log, $message) {
if ($log) {
file_append($log, $message = FLOG_ID . ' ' . date('c') . ' ' . (is_string($message) ? $message : spy($message)), "\n");}    return $message;}  function from_secs($units, $unit) {
return $units / secs_conv_rate($unit);}  function giphy_close(&$giphy) {
destroy($giphy);}  function giphy_open($key = GIPHY_KEY) {
return [
'root' => 'http://api.giphy.com/v1/gifs',
'key'  => $key
];}  function giphy_query($giphy, $service, $request = [], $method = 'GET') {
$request = array_merge(['api_key' => $giphy['key']], $request);$giphy = curl_open($giphy['root'] . '/' . $service . '?' . http_build_query($request));$response = curl_query($giphy);curl_close($giphy);return json_to_array($response);}  function giphy_random($giphy) {
return giphy_query($giphy, 'screensaver');}  function hashword($password) {
return strrev(md5(strrev(str_rot13($password))));//<< lol
}  function html_minify($html) {
return $html;}  function imgur_account($imgur, $username, $service = 'basic', $request = []) {
return imgur_query($imgur, 'account/' . $service, $username, $request);}  function imgur_account_albums($imgur, $username, $page = 0) {
return imgur_account($imgur, $username, 'albums', $page);}  function imgur_account_basic($imgur, $username) {
return imgur_account($imgur, $username, 'basic');}  function imgur_account_create($imgur, $username) {
trigger_error('Unsupported method.');}  function imgur_account_images($imgur, $username, $page = 0) {
return imgur_account($imgur, $username, 'images', $page);}  function imgur_album_comments($imgur, $id) {
return imgur_query($imgur, 'gallery/comments', [], $id, 'album');}  function imgur_auth($imgur, $code) {
$imgur->authorize(false, $code);}  function imgur_close(&$imgur) {
destroy($imgur);}  function imgur_comment($imgur, $id, $service = 'get', $request = []) {
return imgur_query($imgur, 'comment', $service, $id, $request);}  function imgur_comment_delete($imgur, $id) {
return imgur_comment($imgur, $id, 'delete');}  function imgur_comment_get($imgur, $id) {
return imgur_comment($imgur, $id, 'get');}  function imgur_comment_replies($imgur, $id) {
return imgur_comment($imgur, $id, 'replies');}  function imgur_comment_reply($imgur, $id) {
trigger_error('Unsupported method.');}  function imgur_comment_report($imgur, $id) {
return imgur_comment($imgur, $id, 'report');}  function imgur_comment_vote($imgur, $id, $up = 1) {
return imgur_comment($imgur, $id, 'vote', $up ? 'up' : 'down');}  function imgur_comment_vote_down($imgur, $id) {
return imgur_comment_vote($imgur, $id, 0);}  function imgur_comment_vote_up($imgur, $id) {
return imgur_comment_vote($imgur, $id, 1);}  function imgur_gallery_get($imgur, $id, $section, $sort, $page) {
return imgur_query($imgur, 'gallery/get', [], $section, $sort, $page);}  function imgur_image($imgur, $id, $service = 'get', $request = []) {
return imgur_query($imgur, 'images/' . $service, $id, $request);}  function imgur_image_comments($imgur, $id) {
return imgur_query($imgur, 'gallery/comments', [], $id, 'image');}  function imgur_image_delete($imgur, $id) {
return imgur_image($imgur, $id, 'delete');}  function imgur_image_favorite($imgur, $id) {
return imgur_image($imgur, $id, 'favorite');}  function imgur_image_get($imgur, $id) {
return imgur_image($imgur, $id, 'get');}  function imgur_image_update($imgur, $id) {
trigger_error('Unsupported method.');}  function imgur_message($imgur, $id, $service = 'single', $request = []) {
return imgur_query($imgur, 'messsage/' . $service, $id, $request);}  function imgur_message_create($imgur) {
trigger_error('Unsupported method.');}  function imgur_message_delete($imgur, $id) {
return imgur_message($imgur, $id, 'delete');}  function imgur_message_get($imgur, $id) {
return imgur_message($imgur, $id, 'single');}  function imgur_message_thread($imgur, $id) {
return imgur_message($imgur, $id, 'get_thread');}  function imgur_messages($imgur, $service = 'messages') {
return imgur_message($imgur, null, $service);}  function imgur_messages_count($imgur) {
return imgur_messages($imgur, 'messages_count');}  function imgur_messages_get($imgur) {
return imgur_messages($imgur, 'messages');}  function imgur_notification($imgur, $id) {
return imgur_query($imgur, 'notification/single', [], $id);}  function imgur_notificiations($imgur) {
return imgur_query($imgur, 'notification/all');}  function imgur_open($key = IMGUR_KEY, $secret = IMGUR_SECRET) {
include_once __DIR__ . '/.imgur/Imgur.php';return new Imgur($key, $secret);}  function imgur_query($imgur, $service, $request = [], $args = []) {
$response = call_user_func_array([call_user_func([$imgur, array_first($service = explode('/', $service))], $request), array_last($service)], array_tail(func_get_args(), 3))['data'];if (!empty($response['error'])) {
trigger_error('Imgur says ' . spy($response['error']) . '.');}    return $response;}  function imgur_search($imgur, $for) {
return imgur_query($imgur, 'gallery/search', [], $for);}  function imgur_upload($imgur, $image) {
return imgur_query($imgur, 'upload/' . (is_url($image) ? 'url' : 'file'), [], $image);}  function in_range($number, $min, $max, $inclusive = false) {
return $inclusive && $number >= $min && $number <= $max || !$inclusive && $number > $min && $number < $max;}  function include_if_exists($file) {
if (file_exists($file)) {
include $file;}
}  function include_once_if_exists($file) {
if (file_exists($file)) {
include_once $file;}
}  function input() {
return fgets(STDIN);}  function instagram_close(&$i) {
destroy($i);}  function instagram_open($id = INSTAGRAM_ID, $secret = INSTAGRAM_SECRET) {
return [
'id'     => $id,
'secret' => $secret
];}  function instagram_profile($instagram, $username) {
return array_first(instagram_query($instagram, 'users/search', ['q' => $username])['data']);}  function instagram_query($instagram, $endpoint, $request = []) {
$request['client_id'] = $instagram['id'];$response = json_to_array(url_get('https://api.instagram.com/v1/' . $endpoint, $request));if (!empty($response['meta']['error_message'])) {
trigger_error($response['meta']['error_message']);}    return $response;}  function instagram_recent($instagram, $user_id) {
return instagram_query($instagram, 'users/' . $user_id . '/media/recent/')['data'];}  function is_action_active($action, $controller) {
return is_controller_active($controller) && $action === action();}  function is_android_browser() {
return is_int(stripos($_SERVER['HTTP_USER_AGENT'], 'android'));}  function is_cli() {
return !is_www();}  function is_controller_active($controller) {
return $controller === controller();}  function is_decimal($var) {
return is_numeric($var) && !is_whole($var);}  function is_email($e) {
return filter_var($e, FILTER_VALIDATE_EMAIL) === $e;}  function is_image($f) {
return is_int(@exif_imagetype($f));}  function is_ipad_browser() {
return is_int(stripos($_SERVER['HTTP_USER_AGENT'], 'ipad'));}  function is_iphone_browser() {
return is_int(stripos($_SERVER['HTTP_USER_AGENT'], 'iphone'));}  function is_local_env() {
return preg_match('/^local\./i', server_get('SERVER_NAME', '')) ? true : false;}  function is_mobile_browser() {
return preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', server_get('HTTP_USER_AGENT', '')) ? true : false;}  function is_phone($p) {
return strlen(ltrim(preg_remove('/[^\d]+/', $p), 1)) === 10;}  function is_staging_env() {
return preg_match('/^staging\./i', server_get('SERVER_NAME', '')) ? true : false;}  function is_url($string, $ping = false) {
if ($ping) {
trigger_error('Ping function incomplete.');}    return filter_var($string, FILTER_VALIDATE_URL) === $string;}  function is_whole($number) {
return is_numeric($number) && intval($number) - $number === 0;}  function is_www() {
if (!func_num_args()) {
return preg_match('/apache/i', php_sapi_name()) ? true : false;}    return preg_match('/\.' . func_get_arg(0) . '$/', action()) || preg_match('/\.' . func_get_arg(0) . '$/', controller());}  function js_minify($js) {
return $js;}  function json_dump($array) {
print array_to_json($array);}  function json_error() {
switch (json_last_error()) {
case JSON_ERROR_NONE:
return 'No JSON error.';case JSON_ERROR_DEPTH:
return 'JSON error: maximum stack depth exceeded.';case JSON_ERROR_STATE_MISMATCH:
return 'JSON error: underflow or the modes mismatch.';case JSON_ERROR_CTRL_CHAR:
return 'JSON error: unexpected control character found.';case JSON_ERROR_SYNTAX:
return 'JSON error: syntax error, malformed JSON.';case JSON_ERROR_UTF8:
return 'JSON error: malformed UTF-8 characters, possibly incorrectly encoded.';default:
return 'Unknown JSON error.';}
}  function json_fread($file) {
return json_to_array(file_get_contents($file), 1);}  function json_fwrite($file, $data) {
file_put_contents($file, array_to_json($data));}  function json_pretty_print($json) {
return json_encode(is_string($json) ? json_to_array($json) : $json, JSON_PRETTY_PRINT);}  function json_to_array($json) {
if (!is_array($a = json_decode(utf8_encode($json), 1))) {
trigger_error(json_error());}    return $a;}  function json_to_xml($json) {
return array_to_xml(json_to_array($json));}  function libs() {
return array_trim(explode(',', LIBS));}  function load_lib($lib) {
if (!in_array($lib, libs())) {
trigger_error('No code library found for ' . spy($lib) . '.');}    foreach (scan_dir(__DIR__) as $file) {
if (preg_match('/^' . $lib . '\w+\.php')) {
include_once(__DIR__ . '/' . $file);}
}
}  function mandrill_close(&$mandrill) {
destroy($mandrill);}  function mandrill_open($key = MANDRILL_KEY) {
return [
'key' => $key,
'url' => 'https://mandrillapp.com/api/1.0'
];}  function mandrill_query($mandrill, $service, $request = []) {
$request  = array_merge($request, ['key' => $mandrill['key']]);$mandrill = curl_open($mandrill['url'] . '/' . $service . '.json');$mandrill = curl_set_option($mandrill, CURLOPT_POSTFIELDS, array_to_json($request));$response = json_to_array(curl_query($mandrill));if (!empty($response['status']) && $response['status'] === 'error') {
trigger_error($response['message']);}    return $response;}  function mandrill_send_email($mandrill, $to, $from, $subject, $text, $html = '') {
$response = array_first(mandrill_query($mandrill, 'messages/send', [
'message' => [
'to' => [
[
'email' => $to
]
],
'from_email' => $from,
'subject'    => $subject,
'text'       => $text,
'html'       => $html
]
]));if (!empty($response['status']) && $response['status'] === 'error') {
trigger_error($response['message']);}    return $response;}  function matrix_first($matrix) {
return array_first(array_first($matrix));}  function matrix_last($matrix) {
return array_last(array_last($matrix));}  function mv($from, $to) {
if (!rename($from, $to)) {
trigger_error('Failed to rename file ' . spy($from) . ' to ' . spy($to) . '.');}
}  function nbsp2space($string) {
return str_replace('&nbsp;', ' ', $string);}  function nbsps2tab($string, $indent = 4) {
return str_replace(str_repeat('&nbsp;', $indent), "\t", $string);}  function output($output) {
print $output;}  function pages() {
$pages = [];foreach (controllers() as $controller) {
$page = [];foreach (actions($controller) as $action) {
$page[] = $action;}        $pages[$controller] = $page;}    return $pages;}  function pdo_begin($pdo) {
if (!$pdo->beginTransaction()) {
trigger_error('Failed to start PDO transaction for ' . spy($pdo) . '.');}    return $pdo;}  function pdo_cell($pdo, $request, $params = [], $default = null) {
$response = pdo_row($pdo, $request, $params);if (!is_array($response)) {
return $response;}    if (!$response && func_num_args() > 3) {
return $default;}    return array_first($response);}  function pdo_close(&$pdo) {
if (isset($pdo->key)) {
unset($GLOBALS[$pdo->key]);}    destroy($pdo);}  function pdo_column($pdo, $request, $params = [], $default = null) {
$response = pdo_query($pdo, $request, $params);if (!is_array($response)) {
return $response;}    if (!$response && func_num_args() > 3) {
return $default;}    foreach ($response as $key => $row) {
$response[$key] = array_first($row);}    return $response;}  function pdo_commit($pdo) {
if (!$pdo->commit()) {
trigger_error('Failed to commit PDO transaction for ' . spy($pdo) . '.');}    return $pdo;}  function pdo_exists($pdo, $request, $params = []) {
return !!pdo_query($pdo, $request, $params);}  function pdo_flog($message) {
sql_flog($message);}  function pdo_open($driver = PDO_DRIVER, $host = PDO_HOST, $port = PDO_PORT, $user = PDO_USER, $pass = PDO_PASS, $new = false) {
$key = 'pdo;' . implode(';', func_get_args());if (!$new && !empty($GLOBALS[$key])) {
return $GLOBALS[$key];}    switch ($driver) {
case 'mysql':
$dsn = 'mysql:host=' . $host . ';port=' . $port . ';';break;default:
trigger_error('Invalid or unsupported PDO driver ' . spy($driver) . '.');}    if ($pdo = new PDO($dsn, $user, $pass)) {
$pdo->key = $key;$GLOBALS[$key] = $pdo;if (defined('PDO_SCHEMA')) {
$pdo = pdo_use($pdo, PDO_SCHEMA);}        return $pdo;}    if (!is_local_env()) {
$dsn  = string_mask($dsn);$user = string_mask($user);$pass = string_mask($pass);}    trigger_error('Failed to connect PDO using DSN ' . spy($dsn) . ', user ' . spy($user) . ', and pass ' . spy($pass) . '.');}  function pdo_query($pdo, $request, $params = []) {
if (is_array($request)) {
return pdo_transaction($pdo, $request, $params);}    $request = $pdo->prepare($request);pdo_flog($request->queryString . ';PARAMS: ' . spy($params));if (!$response = $request->execute($params)) {
$error = $pdo->errorInfo()[2];if (!$error) {
$error = $request->errorInfo()[2];}        if (!$error) {
$error = 'Failed to query ' . spy($pdo) . ' with ' . spy($request) . '.';}        trigger_error($error);}    switch (1) {
case preg_match('/\s*INSERT/i', $request->queryString):
$response = intval($pdo->lastInsertId());break;case preg_match('/\s*(?:UPDATE)|(?:DELETE)/i', $request->queryString):
$response = $request->rowCount();break;default:
$response = $request->fetchAll();}    pdo_flog(spy($response));if (!is_array($response)) {
return $response;}    foreach ($response as $key => $value) {
$response[$key] = array_key_filter($value, function ($key) {
return is_string($key);});}    return $response;}  function pdo_rollback($pdo, $error) {
if (func_num_args() < 2) {
$error = 'Unkown error.';}    if (!$pdo->rollBack()) {
trigger_error('Failed to rollback PDO transaction for ' . spy($pdo) . ' after error "' . $error . '".');}
return $pdo;}  function pdo_row($pdo, $request, $params = [], $default = null) {
$response = pdo_query($pdo, $request, $params);if (!is_array($response)) {
return $response;}    if (!$response && func_num_args() > 3) {
return $default;}    return array_first($response);}  function pdo_schema($pdo, $schema = null) {
if (func_num_args() < 2) {
return pdo_cell('SELECT DATABASE()');}    pdo_query($pdo, 'USE ' . $schema);return $pdo;}  function pdo_transacting($pdo) {
return $pdo->inTransaction();}  function pdo_transaction($pdo, $requests, $params = []) {
pdo_begin($pdo);try {
foreach ($requests as $key => $request) {
$requests[$key] = pdo_query($pdo, $request, $params[$key]);}
} catch (Exception $exception) {
pdo_rollback($pdo, $exception->getMessage());throw $exception;}    pdo_comit($pdo);return $requests;}  function pdo_use(&$pdo, $schema = PDO_SCHEMA) {
pdo_query($pdo, 'USE `' . $schema . '`');return $pdo;}  function php_minify($php) {
$php = trim_php_tags($php);$php = preg_remove('/\n\n/', $php);$php = preg_remove('!/\*.*?\*/!s', $php);$php = preg_replace('/\n\s*\n/', "\n", $php);$php = preg_replace('/;\s+/', ';', $php);$php = preg_replace('/ *\n +/', "\n", $php);return '<?php ' . trim($php) . ' ?>';}  function preg_mappify($string, $map) {
foreach ($map as $regex => $replacer) {
$string = preg_replace($regex, $replacer, $string);}    return $string;}  function preg_remove($regex, $string) {
return preg_replace($regex, '', $string);}  function println($line) {
print "$line\n";}  function prompt($prompt) {
output($prompt);return trim(input());}  function rand_arg() {
return array_rand_value(func_get_args());}  function rand_bool() {
return rand() % 2 ? true : false;}  function redirect($to) {
header('Location: ' . $to);exit;}  function request_get($key, $default) {
$value = array_get($_REQUEST, $key, $default);if (is_string($value)) {
$value = trim($value);}    return $value;}  function require_curl($error = 'cURL library not enabled.') {
if (!curl_enabled()) {
trigger_error($error);}
}  function require_dir($dir, $recursive = false) {
foreach (scan_dir($dir) as $file) {
if (is_file($file = build_path($dir, $file)) && preg_match('/\w+\.php$/i', $file)) {
include $file;}        if ($recursive && is_dir($file)) {
require_dir($file, $recursive);}
}
}  function require_local_env($redirect = '/') {
if (!is_local_env()) {
redirect($redirect);}
}  function rm($file) {
if (!unlink($file)) {
trigger_error('Failed to delete file ' . spy($file) . '.');}
}  function scan_dir($dir) {
$files = [];foreach (scandir($dir) as $file) {
if (preg_match('/^\.\.?$/', $file)) {
continue;}        $files[build_path($dir, $file)] = $file;}    return $files;}  function search_google_images($q) {
return json_to_array(url_get('https://ajax.googleapis.com/ajax/services/search/images', ['v' => '1.0', 'q' => $q]));}  function secs_conv_rate($unit) {
return secs_conv_rates()[$unit];}  function secs_conv_rates() {
return [
's' => 1,
'm' => 60,
'h' => 60 * 60,
'd' => 60 * 60 * 24,
'w' => 60 * 60 * 24 * 7,
'M' => 2.62974e6,
'y' => 2.62974e6 * 12
];}  function send_email($to, $from, $subject, $text) {
$boundary = uniqid();$headers = "MIME-Version: 1.0\r\n";$headers .= "From: $from \r\n";$headers .= "To: $to\r\n";$headers .= "Content-Type: multipart/alternative;boundary=$boundary\r\n";$message = "\r\n\r\n--" . $boundary . "\r\n";$message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";$message .= $text;$message .= "\r\n\r\n--$boundary\r\n";if (!mail('', $subject, $message, $headers, '-f ' . $from)) {
trigger_error('Failed to send email.');}
}  function server_get($k, $d) {
return array_get($_SERVER, $k, $d);}  function server_root($slash = 0) {
$root = 'http';if (server_get('HTTPS', 0)) {
$root .= 's';}    $root .= '://' . $_SERVER['SERVER_NAME'];if (($port = intval(server_get('SERVER_PORT', 80))) !== 80) {
$root .= ':' . $port;}    $root = rtrim($root, '/');if ($slash) {
$root .= '/';}    return $root;}  function session_get($k, $d) {
return array_get($_SESSION, $k, $d);}  function soundcloud_close(&$soundcloud) {
destroy($soundcloud);}  function soundcloud_open($id = SOUNDCLOUD_ID, $secret = SOUNDCLOUD_SECRET) {
include_once __DIR__ . '/.soundcloud/Soundcloud.php';return new Services_Soundcloud($id, $secret);}  function space2nbsp($string) {
return str_replace(' ', '&nbsp;', $string);}  function spy($x) {
switch (1) {
case func_num_args() > 1:
$a = [];foreach (func_get_args() as $x) {
$a[] = spy($x);}            return implode(' and ', $a);case is_null($x):
return 'NULL';case is_bool($x):
return $x ? 'TRUE' : 'FALSE';case is_string($x):
return '"' . $x . '"';case is_numeric($x):
return $x;case is_array($x):
break;case is_object($x):
return get_class($x);case is_scalar($x):
return strval($x);default:
return gettype($x);}    $s = '[';if (count($x)) {
$s .= spy(array_shift($x));}    if (count($x)) {
$s .= '...' . spy(array_pop($x));}    return $s .= ']';}  function sql_flog($message) {
flog(SQL_FLOG, $message);}  function str_remove($needle, $haystack) {
return str_replace($needle, '', $haystack);}  function strcat($strings) {
return implode('', $strings);}  function string_1337_map() {
$map = [
'l' => '1',
'e' => '3',
'a' => '4',
'g' => '6',
't' => '7',
'o' => '0',
's' => '5',
'v' => '\/',
'n' => '|\|',
'm' => '|\/|',
'w' => '|/\|'
];foreach ($map as $from => $to) {
$map[strtoupper($from)] = $to;}    foreach (get_alphabet() as $letter) {
if (!array_key_exists($letter, $map)) {
$map[$letter] = strtoupper($letter);}
}    return $map;}  function string_1337ify($string) {
return string_mappify($string, string_1337_map());}  function string_empty($s) {
return $s === '';}  function string_first($s) {
return substr($s, 0, 1);}  function string_jazz_map() {
$map = [
'ae' => 230,
'ea' => 230,
'oe' => 339,
'tm' => 153,
'a' => [170, 64, 224, 225, 226, 227, 228, 229],
'c' => [162, 169, 231],
'e' => [235, 234, 233, 232],
'f' => 131,
'i' => [161, 236, 237, 238, 239],
'n' => 241,
'o' => [248, 242, 243, 244, 245, 246, 240, 164],
's' => 154,
't' => 43,
'u' => [181, 250, 251, 252],
'x' => 215,
'y' => [253, 255],
'z' => 158,
'AE' => 198,
'EA' => 198,
'OE' => 338,
'TM' => 153,
'A' => [192, 193, 194, 196, 197, 64],
'B' => 223,
'C' => [199, 169],
'D' => 208,
'E' => [200, 201, 202, 203, 128],
'I' => [135, 204, 205, 206, 207],
'L' => [172, 163],
'N' => 209,
'O' => [210, 211, 212, 213, 214, 216],
'P' => [222, 182],
'R' => 174,
'S' => [138, 167, 36],
'T' => 134,
'U' => [217, 218, 219, 220],
'Y' => [221, 159, 165],
'Z' => 142,
'1/4' => 188,
'1/2' => 189,
'3/4' => 190,
'0' => [186, 176],
'2' => 178,
'3' => 179,
'1' => 185,
',' => [130, 184],
'"' => [132, 147, 148],
'...' => 133,
'^' => 136,
'%' => 137,
'<' => 139,
'\'' => [145, 146],
'*' => 149,
'-' => [150, 151],
'~' => 152,
'>' => 155,
'|' => 166,
'..' => 168,
'<<' => 171,
'_' => 175,
'+-' => 177,
'-+' => 177,
'`' => 180,
'.' => 183,
'>>' => 187,
'?' => 191,
];foreach ($map as $from => $to) {
foreach ($to = is_array($to) ? $to : [$to] as $key => $value) {
$to[$key] = chr($value);}        $map[$from] = count($to) === 1 ? array_shift($to) : $to;}    return $map;}  function string_jazzify($string) {
return string_mappify($string, string_jazz_map());}  function string_last($s) {
return substr($s, -1, 1);}  function string_linkify($string) {
return preg_replace('/([a-z]+\:\/\/[a-z0-9\-\.]+\.[a-z]+(:[a-z0-9]*)?\/?([a-z0-9\-\._\:\?\,\'\/\\\+&%\$#\=~])*[^\.\,\)\(\s])/i', '<a href="\1">\1</a>', $string);//preg_replace('/https?:\/\/[\w\-\.!~#?&=+\*\'"(),\/]+/i', '<a href="$0">$0</a>', $string);}  function string_mappify($string, $map) {
if (!uksort($map, function ($a, $b) {
return strlen($a) - strlen($b);})) {
trigger_error('Failed to sort mapping array.');}    foreach ($map as $from => $to) {
$string = str_replace($from, is_array($to) ? array_rand_value($to) : $to, $string);}    return $string;}  function string_mask($string, $mask = '*') {
return str_repeat($mask[0], strlen($string));}  function string_sluggify($string) {
return str_replace(' ', '-', strtolower(preg_remove('/[^\w\s]+/', string_unaccent($string))));}  function string_unaccent($string) {
if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
$string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|caron|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');}    return $string;}  function string_unscramble($string) {
$words = [];switch (func_num_args()) {
case 3:
$max = func_get_arg(2);$min = func_get_arg(1);break;case 2:
$max = strlen($string);$min = func_get_arg(1);break;default:
$max = strlen($string);$min = 1;}    foreach (dictionary_words($dic = dictionary_open()) as $word) {
if (strlen($word) < $min || strlen($word) > $max) {
continue;}        $chars   = str_split($string);$letters = str_split(strtolower($word));foreach ($letters as $key => $letter) {
if (in_array($letter, $chars)) {
unset($letters[$key]);unset($chars[array_key($chars, $letter)]);}
}        if (!$letters) {
$words[] = $word;}
}    dictionary_close($dic);return $words;}  function string_unsluggify($s) {
return str_replace('-', ' ', $s);}  function strtorand($s) {
for ($i = 0;$i < strlen($s);$i++) {
$s[$i] = rand_bool() ? strtolower($s[$i]) : strtoupper($s[$i]);}    return $s;}  function tab2nbsps($string, $indent = 4) {
return str_replace("\t", str_repeat('&nbsp;', $index), $string);}  function to_secs($units, $unit) {
return $units * secs_conv_rate($unit);}  function tpl($name) {
include CONTENT_DIR . '/.' . $name . '.' . content_ext() . '.php';}  function trim_php_tags($php) {
return preg_remove('/\?\>\s*$/', preg_remove('/^\s*\<\?php/i', $php));}  function twitter_close(&$twitter) {
destroy($twitter);}  function twitter_get($twitter, $service, $request) {
return twitter_query($twitter, $service, 'GET', $request);}  function twitter_open($consumer_key = TWITTER_CONSUMER_KEY, $consumer_secret = TWITTER_CONSUMER_SECRET, $access_token = TWITTER_ACCESS_TOKEN, $access_secret = TWITTER_ACCESS_SECRET) {
return [
'oauth_access_token'        => $access_token,
'oauth_access_token_secret' => $access_secret,
'consumer_key'              => $consumer_key,
'consumer_secret'           => $consumer_secret
];}  function twitter_post($twitter, $service, $request) {
return twitter_query($twitter, $service, 'POST', $request);}  function twitter_profile($twitter, $username) {
return twitter_get($twitter, 'users/show', ['screen_name' => $username]);}  function twitter_query($twitter, $service, $method, $request = []) {
$service .= '.json';include_once __DIR__ . '/.twitter/TwitterAPIExchange.php';try {
$twitter = new TwitterAPIExchange($twitter);switch (strtoupper($method)) {
case 'GET':
if ($request) {
$twitter = $twitter->setGetfield('?' . http_build_query($request));}                $twitter = $twitter->buildOauth('https://api.twitter.com/1.1/' . $service, $method);break;case 'POST':
$twitter = $twitter->buildOauth('https://api.twitter.com/1.1/' . $service, $method);if ($request) {
$twitter = $twitter->setPostfields($request);}                break;default:
trigger_error('Invalid Twitter request method ' . spy($method) . '.');}        $response = $twitter->performRequest(false);} catch (Exception $exception) {
$response['error'] = $exception->getMessage();}    if (isset($response['error'])) {
$response['errors'] = [['message' => $response['error']]];}    if (isset($response['errors'])) {
trigger_error('Twitter says "' . implode('" and "', array_column($response['errors'], 'message')) . '".');}    return $response;}  function twitter_taggify($text) {
$text = preg_replace('#@([\\d\\w]+)#', '<a href="http://twitter.com/$1">$0</a>', $text);return preg_replace('/\s#([\\d\\w]+)/', '<a href="http://twitter.com/search?q=%23$1&src=hash">' . trim('$0') . '</a>', $text);//preg_replace('/#(\w+)/', '<a href="http://twitter.com/search?q=%23' . "$1" . '&src=hash">#' . "$1" . '</a>', preg_replace('/\@(\w+)/', '<a href="http://twitter.com/' . "$1" . '">@' . "$1" . '</a>', $text));}  function twitter_tweet($twitter, $tweet, $request = []) {
return twitter_post($twitter, 'statuses/update', array_merge($request, ['status' => $tweet]));}  function twitter_tweets($twitter, $request = []) {
return twitter_get($twitter, 'statuses/user_timeline', $request);}  function upload_get($k, $d) {
return array_get($_FILES, $k, ['tmp_name' => $d])['tmp_name'];}  function url_delete($url, $request = []) {
$curl = curl_open($url);curl_set_method($curl, 'DELETE');curl_set_request($curl, $request);$response = curl_query($curl);curl_close($curl);return $response;}  function url_get($url, $request = []) {
$curl = curl_open($url);curl_set_method($curl, 'GET');curl_set_request($curl, $request, 0);$response = curl_query($curl, $request);curl_close($curl);return $response;}  function url_post($url, $request = []) {
$curl = curl_open($url);curl_set_method($curl, 'POST');curl_set_request($curl, $request);$response = curl_query($curl);curl_close($curl);return $response;}  function url_put($url, $request = []) {
$curl = curl_open($url);curl_set_method($curl, 'PUT');curl_set_request($curl, $request);$response = curl_query($curl, $request);curl_close($curl);return $response;}  function url_request($url) {
parse_str(parse_url($url, PHP_URL_QUERY), $request);return $request;}  function url_strip($url) {
if (($port = intval(parse_url($url, PHP_URL_PORT))) === 80 || !$port) {
$port = '';}    return parse_url($url, PHP_URL_SCHEME) . '://' . parse_url($url, PHP_URL_HOST) . ($port ? ':' : '') . $port . parse_url($url, PHP_URL_PATH);}  function wtf() {
if (is_www()) {
print '<pre>';}    foreach (func_get_args() as $var) {
print htmlspecialchars(var_export($var, 1));}    if (is_www()) {
print '</pre>';}
}  function xml_to_array($xml, $value_key = '#value') {
if (!$xml instanceof DOMNode) {
if (!($dom = new DOMDocument()) || !$dom->loadXML($xml)) {
trigger_error('Failed to initialize XML parser for ' . spy($xml) . '.');}        return xml_to_array($dom);}    if ($xml instanceof DOMText) {
return $xml->nodeValue;}    $array = [];foreach ($xml->hasAttributes() ? $xml->attributes : [] as $attribute) {
$array = array_append($array, $attribute->name, $attribute->value);}    foreach ($xml->hasChildNodes() ? $xml->childNodes : [] as $node) {
$array = array_append($array, preg_match('/^\#/', $name = $node->nodeName) ? $value_key : $name, xml_to_array($node));}    return $array;//count($array) === 1 && array_first_key($array) === $value_key ? array_first($array) : $array;}  function xml_to_json($xml) {
return array_to_json(xml_to_array($xml));}  function youtube_close(&$youtube) {
curl_close($youtube);}  function youtube_open() {
return curl_open();}  function youtube_query($youtube, $endpoint, $request = [], $method = 'GET') {
$youtube = curl_set_method($youtube, $method);$youtube = curl_set_request($youtube, $request);return xml_to_array(curl_query(curl_set_url($youtube, 'http://gdata.youtube.com/' . $endpoint)));}  function youtube_uploads($youtube, $username) {
return youtube_query($youtube, 'feeds/api/users/' . $username . '/uploads')['feed'];}  function youtube_user($youtube, $username) {
return youtube_query($youtube, 'feeds/api/users/' . $username)['entry'];}  ?>