<?php
function get_menus() {
    $menus = [];

    $menus['home'] = [
        'title' => 'home',
        'url'   => server_root()
    ];

    foreach (get_cqcons() as $cqcon) {
        $menu = [
            'title' => $cqcon
        ];

        if (!$cqacts = get_cqacts($cqcon)) {
            $menus[] = $menu;

            break;
        }

        $items = [];

        foreach ($cqacts as $cqact) {
            $items[] = [
                'title' => $cqact,
                'url'   => server_root() . '/' . $cqcon . '/' . $cqact
            ];
        }

        $menu['items'] = $items;

        $menus[] = $menu;
    }

    $menus[] = [
        'title' => 'source',
        'url'   => 'https://bitbucket.org/chaseisabelle/curlyq/src'
    ];

    return $menus;
}
?>

