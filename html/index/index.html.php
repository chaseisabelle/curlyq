<div class="col-md-8 col-md-offset-2">
    <div class="row">
        <div class="col-md-12">
            <center>
                <p>curlyq is a RESTful abstraction for MySQL databases. This is an example of it in action.</p>
                <p>Feel free to <a href="https://bitbucket.org/chaseisabelle/curlyq/src">download</a> the source code and hack it to peices!</p>
            </center>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-12">
            <form id="request-form">
                <input id="request-input" type="text" value="" required placeholder="request..." class="form-control" />
            </form>
            <br />
            <textarea id="response-textarea" placeholder="...response" readonly class="form-control"></textarea>
        </div>
    </div>
</div>
